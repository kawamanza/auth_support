AuthSupport::Engine.routes.draw do
  # Registration
  get "signup" => "registration#index", as: :signup
  post "signup" => "registration#create"
  get "register" => "registration#new", as: :register
  post "register" => "registration#register"

  # Login
  get "signin" => "identification#index", as: :signin
  post "signin" => "identification#create_session"

  # Logout
  get "signout" => "identification#destroy_session", as: :signout

  # Replace user e-mail
  get "account/change-email" => "accounts#new_email", as: :change_email
  post "account/change-email" => "accounts#create_temp_email", as: nil
  get "account/change-email/:token" => "accounts#validate_email", as: :validate_email
  post "account/change-email/:token" => "accounts#create_email"

  # Replace password
  get "password/edit" => "accounts#edit_password", as: :edit_password
  put "password/edit" => "accounts#update_password", as: nil

  # Reset password
  get "password/reset" => "identification#reset_password", as: :reset_password
  post  "password/reset" => "identification#create_reset_password_link"
  get "password/reset/:token" => "identification#reset_password_validation", as: :validate_reset_password
  put "password/reset/:token" => "identification#update_password"
end
