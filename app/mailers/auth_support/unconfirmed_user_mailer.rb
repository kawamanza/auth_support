module AuthSupport
  class UnconfirmedUserMailer < ::ApplicationMailer

    def welcome(user, confirmation_url)
      @user = user
      @confirmation_url = confirmation_url
      
      mail(subject: t('auth_support.mailer.welcome.subject', default: 'Welcome'),
        to: user.email)
    end

    def change_email(user, confirmation_url)
      @user = user
      @confirmation_url = confirmation_url
      mail(subject: t('auth_support.mailer.change_email.subject', default: 'E-mail change'),
        to: user.email)
    end

    def reset_password_link(user, confirmation_url)
      @user = user
      @confirmation_url = confirmation_url
      mail(subject: t('auth_support.mailer.reset_password_link.subject', default: 'Reset password'),
        to: user.email)
    end

    # Subject can be set in your I18n file at config/locales/en.yml
    # with the following lookup:
    #
    #   en.unconfirmed_user_mailer.new_user.subject
    #
    def new_user
      @greeting = "Hi"

      mail to: "to@example.org"
    end
  end
end
