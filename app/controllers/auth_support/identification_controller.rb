module AuthSupport
  class IdentificationController < ::ApplicationController
    include CookieMethods
    before_action :load_reset_password_request, only: %i[reset_password_validation update_password]

    # GET /signin
    def index
      expires_in 0, must_revalidate: true
    end

    # POST /signin
    def create_session
      respond_to do |format|
        if sign_in!
          format.html { redirect_to after_signin_url }
        else
          flash.now[:error] = t("auth_support.failure.invalid")
          format.html { render action: 'index' }
        end
      end
    end

    # GET /signout
    def destroy_session
      cookie_token = cookies[:_session_token]
      remove_session_cookies!
      secret = Secret.new
      secret.parse_key(cookie_token) if cookie_token.is_a?(String)
      user = secret.build_user
      respond_to do |format|
        if user && user.email.present?
          AuthenticatedSession.delete_session(secret.token, user.email)
        end
        format.html { redirect_to signin_path }
      end
    end

    # GET /password/reset
    def reset_password
    end

    # POST /password/reset
    def create_reset_password_link
      respond_to do |format|
        if store_reset_link!
          UnconfirmedUserMailer.reset_password_link(@user, validate_reset_password_url(token: @user.validation_token, email: @user.email)).deliver_later
          flash[:notice] = t 'auth_support.notice.reset_link_sent'
          format.html { redirect_to signin_path }
        else
          flash.now[:error] = t 'auth_support.failure.invalid_email'
          format.html { render 'reset_password' }
        end
      end
    end

    # GET /password/reset/:token?email=
    def reset_password_validation
    end

    # PUT /password/reset/:token
    def update_password
      failure = password_params[:password].blank? ?
        :new_password :
        (password_params[:password] != password_params[:password_confirmation]) ?
        :password_confirmation :
        nil
      respond_to do |format|
        if failure.nil? && store_new_password!
          flash[:notice] = t('auth_support.notice.password_changed')
          format.html{ redirect_to signout_path }
        else
          flash.now[:error] = t("auth_support.failure.#{ failure || 'new_password' }")
          format.html{ render 'reset_password_validation' }
        end
      end
    end

  protected
    def after_signin_url
      params[:ret_url].presence || main_app.root_path
    end

    def sign_in!
      @user = User.find_by_email signin_params[:email]
      return false if @user.nil?
      @user.check_password?(signin_params[:password]).tap do |signed_in|
        secret = @user.store_session!(signed_in, request.ip) # TODO: verify header
        create_session_cookies!(secret) if signed_in
      end
    end

    def store_reset_link!
      user = User.find_by_email account_params[:email]
      return false if user.nil?
      @user = UnconfirmedUser.new(email: user.email, password: 'reset_password')
      @user.name = user.name
      @user.person_id = user.person_id
      ActiveRecord::Base.transaction do
        UnconfirmedUser.where(email: user.email).delete_all
        @user.save || raise(ActiveRecord::Rollback)
      end
    end

    def store_new_password!
      user = User.find_by_email @user.email
      return false if user.person_id.present? && user.person_id != @user.person_id
      user.password = account_params[:password]
      ActiveRecord::Base.transaction do
        UnconfirmedUser.where(email: user.email).delete_all
        user.save || raise(ActiveRecord::Rollback)
      end
    end

    def load_reset_password_request
      email = params[:email].presence || account_params[:email]
      @user = UnconfirmedUser.find_by(email: email, validation_token: params[:token])
      if @user.nil? || ! @user.verify_validation_token('reset_password', params[:token])
        flash[:error] = t 'auth_support.failure.reset_password_token_invalid'
        redirect_to reset_password_path
      end
    end

    def signin_params
      params.require(:account)
    end

    def password_params
      @password_params ||= params.require(:account).
        permit(:password, :new_password, :password_confirmation)
    end

    def account_params
      @account_params ||= params.require(:account).permit(:email, :password)
    end
  end
end
