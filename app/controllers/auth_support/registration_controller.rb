# require_dependency "auth_support/application_controller"

module AuthSupport
  class RegistrationController < ::ApplicationController
    include AuthSupport::Controller
    include AuthSupport::UserPersistence

    # GET /signup
    def index
    end

    # GET /email-change
    def index_email_change
      if current_user.nil? || current_user.person_id.nil?
        redirect_to(signin_path, notice: t('auth_support.notice.email_change'))
      end
    end

    # POST /signup
    def create
      build_resource
      respond_to do |format|
        if check_robots(@user) && store_resource
          UnconfirmedUserMailer.welcome(resource, register_url).deliver_later
          format.html { redirect_to signup_path, notice: t("auth_support.notice.confirmation_sent") }
        else
          flash[:error] = @user.errors[:base].first if @user.errors.key?(:base)
          format.html { render action: 'index' }
        end
      end
    end

    # GET /register
    def new
      expires_in 0, must_revalidate: true
    end

    # POST /register
    def register
      respond_to do |format|
        if confirm_and_create_user(params[:account] || {}) 
          format.html { redirect_to signin_path, notice: t("auth_support.notice.user_created") }
        else
          flash.now[:error] = t("auth_support.failure.registration", default: "Confirmation error")
          format.html { render action: 'new' }
        end
      end
    end

  protected

    ## override to make some additional steps
    def store_resource
      if current_user.present? && "true" == params[:email_change]
        resource.person_id = current_user.person_id
      end
      ActiveRecord::Base.transaction do
        UnconfirmedUser.where(email: resource.email).delete_all
        resource.save
      end
    end

    def check_robots(resource)
      return true unless respond_to? :is_robot?
      is_robot? resource
    end

  private

    def build_resource(params = signup_params)
      @user = UnconfirmedUser.new params
    end

    def resource
      @user
    end

    def signup_params
      params.require(:account).permit(:name, :email, :password)
    end
  end
end
