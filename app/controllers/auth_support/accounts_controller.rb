module AuthSupport
  class AccountsController < ::ApplicationController
    before_action :authenticate
    before_action :load_email_change_request, only: %i[validate_email create_email]

    # GET /account/change-email
    def new_email
      respond_to do |format|
        format.html
      end
    end

    # POST /account/change-email
    def create_temp_email
      @user = User.find_by(id: current_user.id)
      email = account_params[:email]
      failure = (account_params[:password].blank? ||
          ! @user.check_password?(account_params[:password])) ?
        :password_match :
        (email.blank? || current_user.email == email) ?
        :invalid_email :
        (email != params[:account][:email_confirmation]) ?
        :email_confirmation :
        (User.where(email: email, person_id: current_user.person_id).exists?) ?
        :email_associated :
        nil
      respond_to do |format|
        @user = UnconfirmedUser.new account_params
        @user.person_id = current_user.person_id
        @user.name = current_user.name
        if failure.nil? && store_resource
          UnconfirmedUserMailer.change_email(@user, validate_email_url(token: @user.validation_token)).deliver_later
          flash[:notice] = t('auth_support.notice.confirmation_sent')
          format.html{ redirect_to main_app.root_path }
        else
          flash.now[:error] = t("auth_support.failure.#{ failure || 'invalid_email' }")
          format.html{ render 'new_email' }
        end
      end
    end

    # GET /account/change-email/:token
    def validate_email
      expires_in 0, must_revalidate: true
    end

    # POST /account/change-email/:token
    def create_email
      @curr_user = User.find(current_user.id)
      failure = (account_params[:password].blank? ||
          ! @curr_user.check_password?(account_params[:password])) ?
        :password_match :
        nil
      respond_to do |format|
        if failure.nil? && store_user
          flash[:notice] = t('auth_support.notice.email_changed')
          format.html{ redirect_to signout_path }
        else
          flash.now[:error] = t("auth_support.failure.#{ failure || 'invalid_email' }")
          format.html{ render 'new_email' }
        end
      end
    end

    # GET /password/edit
    def edit_password
      @user = current_user
      render 'edit_password'
    end

    # PUT /password/edit
    def update_password
      @user = User.find_by(id: current_user.id)
      failure = (password_params[:password].blank? ||
          ! @user.check_password?(password_params[:password])) ?
        :password_match :
        password_params[:new_password].blank? ?
        :new_password :
        (password_params[:new_password] != password_params[:password_confirmation]) ?
        :password_confirmation :
        nil
      respond_to do |format|
        @user.password = password_params[:new_password]
        if failure.nil? && @user.save
          flash[:notice] = t('auth_support.notice.password_changed')
          format.html{ redirect_to signout_path }
        else
          flash.now[:error] = t("auth_support.failure.#{ failure || 'new_password' }")
          format.html{ render 'edit_password' }
        end
      end
    end

    private

      def store_resource
        ActiveRecord::Base.transaction do
          UnconfirmedUser.where(email: @user.email).delete_all
          @user.save || raise(ActiveRecord::Rollback)
        end
      end

      def store_user
        ActiveRecord::Base.transaction do
          UnconfirmedUser.where(email: @user.email).delete_all
          User.where(email: @user.email, person_id: nil).
            where.not(id: @curr_user.id).delete_all
          @curr_user.update_attributes(email: @user.email) || raise(ActiveRecord::Rollback)
        end
      end

      def load_email_change_request
        @user = UnconfirmedUser.where(person_id: current_user.person_id)
                               .where(validation_token: params[:token]).first
        if @user.nil?
          flash[:error] = t 'auth_support.failure.email_validation_token_invalid'
          redirect_to change_email_path
        end
      end

      def password_params
        @password_params ||= params.require(:account).
          permit(:password, :new_password, :password_confirmation)
      end

      def account_params
        @account_params ||= params.require(:account).permit(:email, :password)
      end
  end
end
