class AuthenticatedSession < ActiveRecord::Base
  belongs_to :user

  def self.increase_seq(token, email, prev_seq)
    where(email: email, token: token, seq: prev_seq).
      update_all(seq: prev_seq+1, updated_at: Time.now)
  end

  def self.delete_session(token, email)
  	where(email: email, token: token).delete_all
  end
end
