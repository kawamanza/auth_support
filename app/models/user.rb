require "bcrypt"

class User < ActiveRecord::Base
  belongs_to :person
  attr_accessor :password

  validates_presence_of :email
  validates_format_of :email, allow_nil: true, allow_blank: true, with: /\A[^@\s]+@(?:[-a-z0-9]+\.)+[a-z]{2,}\Z/i
  validates_uniqueness_of :email, unless: proc { errors.key?(:email) }
  validates :password, presence: { on: :create }, format: { allow_blank: true, with: /\A\S{6,}\z/ }
  validates_presence_of :accepted_contract, if: proc { new_record? || accepted_contract_was.present? }

  before_save :generate_encrypted_password

  attr_accessible :name
  attr_accessible :email
  attr_accessible :password
  attr_accessible :accepted_contract

  def check_password?(password)
    bcrypt = ::BCrypt::Password.new(encrypted_password)
    password = ::BCrypt::Engine.hash_secret(password, bcrypt.salt)
    secure_compare password, encrypted_password
  end

  def store_session!(signed_in, request_ip)
    AuthSupport::Secret.new.tap do |secret|
      ActiveRecord::Base.transaction do
        time = create_user_authlog!(signed_in, request_ip)
        if signed_in
          secret.build_key(self, time)
          create_authenticated_session!(secret.token)
        end
      end
    end
  end

  private

  def create_user_authlog!(signed_in, request_ip)
    Time.now.tap do |time|
      log = UserAuthlog.new
      log.user = self
      log.signin_ip = request_ip
      log.signin_at = time
      log.failed_attempt = ! signed_in
      log.save!
    end
  end

  def create_authenticated_session!(token)
    _session = AuthenticatedSession.new
    _session.email = email
    _session.user = self
    _session.token = token
    _session.save!
  end

  def secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end

  def generate_encrypted_password
    if password.present?
      self.encrypted_password = ::BCrypt::Password.create(password, cost: 10)
    end
  end
end
