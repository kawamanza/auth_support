class UnconfirmedUser < ActiveRecord::Base
  self.primary_key = :email
  belongs_to :person
  attr_accessor :password
  attr_accessor :email_confirmation
  attr_accessor :accepted_contract

  validates_presence_of :email, :name
  validates_format_of :email, allow_nil: true, allow_blank: true, with: /\A[^@\s]+@(?:[-a-z0-9]+\.)+[a-z]{2,}\Z/i
  validates_uniqueness_of :email, unless: proc { errors.key?(:email) }
  validates_presence_of :password

  before_save :generate_validation_token

  attr_accessible :email, :name
  attr_accessible :password

  def verify_validation_token(key, token)
    validation_token == digest_validation_token(key) &&
    validation_token == token
  end

  private

  def generate_validation_token
    return if password.blank?
    self.updated_at = Time.now
    self.validation_token = digest_validation_token(password)
  end

  def digest_validation_token(key)
    base = "#{email}/#{key}/#{updated_at.xmlschema}"
    # ::Digest::SHA1.hexdigest(base)
    OpenSSL::HMAC.new(base, "sha1").to_s
  end
end
