require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user){ FactoryGirl.build(:user) }
  it "should save" do
    expect{ user.save }.not_to change{ user.errors.to_hash }
  end
end
