require 'rails_helper'

RSpec.describe UnconfirmedUser, type: :model do
  subject { FactoryGirl.build(:unconfirmed_user) }
  it { should be_kind_of described_class }

  context "mass assignment" do
    let(:user_attrs){ FactoryGirl.attributes_for(:unconfirmed_user) }
    subject { described_class.new user_attrs }
    it { expect(subject.name).not_to be_nil }
    it { expect(subject.email).not_to be_nil }
    it { expect(subject.password).not_to be_nil }
    it { expect(subject.validation_token).to be_nil }
  end

  context "validation" do
    it { is_expected.to be_valid }
    it "should keep validation_token up to date" do
      expect{ subject.save }.not_to change{ subject.errors.to_hash }
      user = described_class.find_by_email subject.email
      expect(user.verify_validation_token(subject.password, subject.validation_token)).
        to eq true
      user.password = subject.password
      Timecop.travel(1.minute.from_now){ user.save }
      token = user.validation_token
      user = described_class.find_by_email subject.email
      expect(user.verify_validation_token(subject.password, subject.validation_token)).
        to eq false
      expect(user.verify_validation_token(subject.password, token)).
        to eq true
    end
  end

  context "validation_token generation" do
    let(:user){ FactoryGirl.build(:unconfirmed_user, email: "MyString") }
    before { user.updated_at = Time.at(1455225872) }
    subject { user.send :digest_validation_token, "key" }
    it { is_expected.to eq "041c2a42fca2ad671f7c8e2405439c844b2d214a" }
  end
end
