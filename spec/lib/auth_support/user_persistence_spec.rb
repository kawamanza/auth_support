require "rails_helper"

module AuthSupport
  RSpec.describe UserPersistence do
    let(:test_class) { Class.new { include UserPersistence } }
    it { expect(test_class).to include described_class }

    context "previous registration to confirm" do
      let(:persistence){ test_class.new }
      let(:params){ FactoryGirl.attributes_for :unconfirmed_user }
      let(:user){ FactoryGirl.build :unconfirmed_user, params }
      before do
        expect{ user.save }.to change(UnconfirmedUser, :count)
        params[:validation_token] = user.validation_token
      end
      subject { persistence.send(:confirm_and_create_user, params) }
      it { expect{ subject }.to change(User, :count) }
      it { is_expected.to eq true }
    end
  end
end
