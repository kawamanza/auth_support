require "rails_helper"

module AuthSupport
  RSpec.describe Secret do
    let(:user){ FactoryGirl.build(:user) }
    let(:secret){ described_class.new }
    let(:now){ Time.now }
    it 'should change cookies values when person_id changes' do
      expect{ user.save }.not_to change{ user.errors.to_hash }
      secret.build_key user, now
      expect(secret.cookie_check(1)).to match(/\A[^\-]+-1-/)
      expect(secret.key[0]).to eq "1"
      expect(secret.key[2]).to eq ""
      user.person_id = 2
      old_token = secret.token
      secret.build_key user
      expect(secret.token).to eq(old_token)
      expect(secret.cookie_check).to match(/\A[^\-]+-1-/)
      expect(secret.key[0]).to eq "1"
      expect(secret.key[2]).to eq "2"
    end
  end
end
