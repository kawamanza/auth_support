module I18nTestHelper
  def i18n(*args)
    opts = args.extract_options!.reverse_merge(:default => "I18n missing")
    if args.first.is_a?(Symbol)
      opts[:default] = Array.wrap(opts[:default])
      if args.first.to_s =~ /\A([^\.]+?)\.([^\.]+?)\.([^\.]+?)\z/
        opts[:default].unshift :"errors.messages.#{$3}"
        opts[:default].unshift :"activerecord.errors.messages.#{$3}"
        args[0] = :"activerecord.errors.models.#{$1}.attributes.#{$2}.#{$3}"
      else
        opts[:default].unshift :"errors.messages.#{args[0]}"
        args[0] = :"activerecord.errors.messages.#{args[0]}"
      end
    end
    args << opts
    I18n.t(*args)
  end
end

RSpec.configure do |config|
  config.include I18nTestHelper, type: :controller
end
