
# Monkey patch recommended on
#  https://blog.pivotal.io/labs/labs/writing-rails-engine-rspec-controller-tests
module EngineControllerTestMonkeyPatch
  extend ActiveSupport::Concern
  included do
    routes { AuthSupport::Engine.routes }
  end
end

RSpec.configure do |config|
  config.include EngineControllerTestMonkeyPatch, type: :controller
end
