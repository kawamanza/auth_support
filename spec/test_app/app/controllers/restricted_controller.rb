class RestrictedController < ApplicationController
  include AuthSupport::Controller
  before_filter :authenticate

  def index
  end
end
