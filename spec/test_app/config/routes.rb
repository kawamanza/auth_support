Rails.application.routes.draw do
  root "application#index"
  mount AuthSupport::Engine => ""
  get "signin" => "auth_support/identification#index", as: :signin
  get "restricted" => "restricted#index", as: :restricted
end
