class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name, limit: 200, null: false

      t.timestamps null: false
    end
  end
end
