require "rails_helper"

RSpec.describe RestrictedController, type: :controller do
  routes { TestApp::Application.routes }
  let(:user){ FactoryGirl.build :user }
  
  it { expect(get: "/restricted").to route_to(controller: "restricted", action: "index")}

  it "requires authentication" do
    get :index
    expect(response).to redirect_to signin_path(ret_url: 'http://test.host/restricted')
  end

  it "identifies signed in user" do
    expect{ user.save }.not_to change{ user.errors.to_hash }
    secret = user.store_session!(true, "0.0.0.0")
    request.cookies[:_session_token] = secret.cookie_token
    request.cookies[:_session_check] = secret.cookie_check
    get :index
    expect(response).to have_http_status :ok
    expect(response).to render_template :index
    expect(response.headers['Set-Cookie']).not_to match(/^_session_token=[^;]+$/)
    expect(response.headers['Set-Cookie']).not_to match(/^_session_check=[^;]+$/)
  end

  it "identifies signed in user with stale session_check" do
    Timecop.travel(10.minutes.ago) do
      expect{ user.save }.not_to change{ user.errors.to_hash }
      secret = user.store_session!(true, "0.0.0.0")
      request.cookies[:_session_token] = secret.cookie_token
      request.cookies[:_session_check] = secret.cookie_check
    end
    get :index
    expect(response).to have_http_status :ok
    expect(response).to render_template :index
    expect(response.headers['Set-Cookie']).not_to match(/^_session_token=[^;]+; path=\/$/)
    expect(response.headers['Set-Cookie']).to match(/^_session_check=[^;]+; path=\/; HttpOnly$/)
  end

  it "identifies signed in user with invalid session_check" do
    expect{ user.save }.not_to change{ user.errors.to_hash }
    secret = user.store_session!(true, "0.0.0.0")
    request.cookies[:_session_token] = secret.cookie_token
    request.cookies[:_session_check] = secret.cookie_check+"0"
    get :index
    expect(response).to redirect_to signin_path(ret_url: 'http://test.host/restricted')
    expect(response.headers['Set-Cookie']).to match(/^_session_token=; path=\/; max-age=0/)
    expect(response.headers['Set-Cookie']).to match(/^_session_check=; path=\/; max-age=0/)
  end

  it "identifies signed in user with shared session_token" do
    Timecop.travel(10.minutes.ago) do
      expect{ user.save }.not_to change{ user.errors.to_hash }
      secret = user.store_session!(true, "0.0.0.0")
      request.cookies[:_session_token] = secret.cookie_token
      request.cookies[:_session_check] = secret.cookie_check
      AuthenticatedSession.increase_seq(secret.token, user.email, 0)
    end
    get :index
    expect(response).to redirect_to signin_path(ret_url: 'http://test.host/restricted')
    expect(response.headers['Set-Cookie']).to match(/^_session_token=; path=\/; max-age=0/)
    expect(response.headers['Set-Cookie']).to match(/^_session_check=; path=\/; max-age=0/)
  end
end
