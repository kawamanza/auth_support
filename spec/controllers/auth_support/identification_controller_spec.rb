require "rails_helper"

module AuthSupport
  RSpec.describe IdentificationController, type: :controller do
    context "GET /signin" do
      it "should load the page" do
        expect(get: signin_path).to route_to(controller: "auth_support/identification", action:"index")
        get :index
      end
    end

    context "POST /signin" do
      let(:user) { FactoryGirl.build :user }
      it "should create session" do
        expect(post: signin_path).to route_to(controller: "auth_support/identification", action: "create_session")
        expect{ user.save }.not_to change{ user.errors.to_hash }
        expect {
          post :create_session, params: { account: { email: user.email, password: "123456" } }
          expect(response.headers['Set-Cookie']).to match(/^_session_token=[^;]+; path=\/$/)
          expect(response.headers['Set-Cookie']).to match(/^_session_check=[^;]+; path=\/; HttpOnly$/)
          expect(response.cookies['_session_token']).not_to be_nil
          expect(response.cookies['_session_check']).not_to be_nil
        }.to change{ UserAuthlog.where(user_id: user.id, failed_attempt: false).count }.by(1)
      end
      it "should fail to create session" do
        expect{ user.save }.not_to change{ user.errors.to_hash }
        expect {
          post :create_session, params: { account: { email: user.email, password: "654321" } }
          expect(response.headers['Set-Cookie']).to be_nil
          expect(flash[:error]).to eq(i18n("auth_support.failure.invalid"))
        }.to change{ UserAuthlog.where(user_id: user.id, failed_attempt: true).count }.by(1)
      end
    end

    context "GET /signout" do
      let(:user) { FactoryGirl.build :user }
      it "should signout the user" do
        expect{ user.save }.not_to change{ user.errors.to_hash }
        secret = user.store_session!(true, "0.0.0.0")
        request.cookies[:_session_token] = secret.cookie_token
        request.cookies[:_session_check] = secret.cookie_check
        expect(get: signout_path).to route_to(controller: "auth_support/identification", action:"destroy_session")
        expect {
          get :destroy_session
          expect(response).to redirect_to signin_path
        }.to change(AuthenticatedSession, :count).by(-1)
      end
    end
  end
end
