require "rails_helper"

module AuthSupport
  RSpec.describe RegistrationController, type: :controller do
    context "GET /signup" do
      it "should load the page" do
        expect(get: signup_path).to route_to(controller: "auth_support/registration", action:"index")
        get :index
      end
    end

    context "GET /email-change" do
      let(:user){ FactoryGirl.build :user }
      let(:person){ FactoryGirl.build :person }
      it { expect(get: email_change_path).to route_to(controller: "auth_support/registration", action: "index_email_change") }
      it "should require authentication to change email" do
        get :index_email_change
        expect(response).to redirect_to(signin_path)
        expect(flash[:notice]).to eq(i18n('auth_support.notice.email_change'))
      end
      it "should require person authentication to change email" do
        expect{ user.save }.not_to change{ user.errors.to_hash }
        secret = user.store_session!(true, "0.0.0.0")
        request.cookies[:_session_token] = secret.cookie_token
        request.cookies[:_session_check] = secret.cookie_check
        get :index_email_change
        expect(response).to redirect_to(signin_path)
      end
      it "should render form if person authenticated" do
        expect{ person.save }.not_to change{ person.errors.to_hash }
        user.person = person
        expect{ user.save }.not_to change{ user.errors.to_hash }
        secret = user.store_session!(true, "0.0.0.0")
        request.cookies[:_session_token] = secret.cookie_token
        request.cookies[:_session_check] = secret.cookie_check
        get :index_email_change
        expect(response).to render_template(:index_email_change)
      end
    end

    context "POST /signup" do
      let(:user){ FactoryGirl.build :user }
      let(:person){ FactoryGirl.build :person }
      let(:unconfirmed_user_attrs){ { name: "John", email: "john@mailinator.com", password: "123456" } }
      it { expect(post: signup_path).to route_to(controller: "auth_support/registration", action: "create") }
      it "should create unconfirmed_user" do
        mailer = double UnconfirmedUserMailer
        expect(mailer).to receive(:deliver_later).once
        expect(UnconfirmedUserMailer).to receive(:welcome).and_return(mailer).once
        expect {
          post :create, params: { account: unconfirmed_user_attrs }
          expect(flash[:notice]).to eq(i18n("auth_support.notice.confirmation_sent"))
        }.to change(UnconfirmedUser, :count).by(1)
      end
      it "should create unconfirmed_user and set person_id if present in session" do
        expect{ person.save }.not_to change{ person.errors.to_hash }
        user.person = person
        expect{ user.save }.not_to change{ user.errors.to_hash }
        secret = user.store_session!(true, "0.0.0.0")
        request.cookies[:_session_token] = secret.cookie_token
        request.cookies[:_session_check] = secret.cookie_check
        mailer = double UnconfirmedUserMailer
        expect(mailer).to receive(:deliver_later).once
        expect(UnconfirmedUserMailer).to receive(:welcome).and_return(mailer).once
        expect {
          post :create, params: { email_change: "true", account: unconfirmed_user_attrs }
        }.to change(UnconfirmedUser, :count).by(1)
        new_user = UnconfirmedUser.find_by_email unconfirmed_user_attrs[:email]
        expect(new_user.person_id).to eq(person.id)
      end
      it "should update unconfirmed_user" do
        unconfirmed_user = FactoryGirl.build(:unconfirmed_user, unconfirmed_user_attrs)
        unconfirmed_user.password = "123123"

        expect{ Timecop.travel(1.minute.from_now){ unconfirmed_user.save } }.
          not_to change{ unconfirmed_user.errors.to_hash }
        mailer = double UnconfirmedUserMailer
        expect(mailer).to receive(:deliver_later).once
        expect(UnconfirmedUserMailer).to receive(:welcome).and_return(mailer).once
        expect {
          post :create, params: { account: unconfirmed_user_attrs }
          expect(response).to redirect_to signup_path
        }.not_to change(UnconfirmedUser, :count)
        new_user = UnconfirmedUser.find_by_email unconfirmed_user.email
        expect(new_user.verify_validation_token("123456", new_user.validation_token)).
          to be_truthy
      end
    end

    context "GET /register" do
      it "should load the page" do
        expect(get: register_path).to route_to(controller: "auth_support/registration", action: "new")
        get :new
        expect(response.headers["Cache-Control"]).to eq("max-age=0, private, must-revalidate")
      end
    end

    context "POST /register" do
      let(:user){ FactoryGirl.build :user }
      let(:person){ FactoryGirl.build :person }
      let(:another_person){ FactoryGirl.build :person }
      let(:register_params){ FactoryGirl.attributes_for :unconfirmed_user }
      it { expect(post: register_path).to route_to(controller: "auth_support/registration", action: "register") }
      it "should create user" do
        user = UnconfirmedUser.new register_params
        expect{ Timecop.travel(1.minute.from_now){ user.save } }.not_to change{ user.errors.to_hash }
        register_params[:validation_token] = user.validation_token
        expect {
          post :register, params: { account: register_params }
        }.to change(User, :count).by(1)
        expect(response).to redirect_to signin_path
        expect(flash[:notice]).to eq(i18n("auth_support.notice.user_created"))
        expect(UnconfirmedUser.find_by_email(user.email)).to be_nil
        expect(User.find_by_email(user.email).person_id).to be_nil
      end
      it "should create user from existing person_id" do
        user = UnconfirmedUser.new register_params
        expect{ person.save }.not_to change{ person.errors.to_hash }
        user.person = person
        expect{ Timecop.travel(1.minute.from_now){ user.save } }.not_to change{ user.errors.to_hash }
        register_params[:validation_token] = user.validation_token
        expect {
          post :register, params: { account: register_params }
        }.to change(User, :count).by(1)
        expect(response).to redirect_to signin_path
        expect(UnconfirmedUser.find_by_email(user.email)).to be_nil
        expect(User.find_by_email(user.email).person_id).to eq(person.id)
      end
      it "should update the person_id of an user unassigned to a person" do
        _user = UnconfirmedUser.new register_params
        expect{ person.save }.not_to change{ person.errors.to_hash }
        user.email = _user.email
        expect{ Timecop.travel(2.minute.from_now){ user.save } }.not_to change{ user.errors.to_hash }
        _user.person = person
        expect{ Timecop.travel(1.minute.from_now){ _user.save } }.not_to change{ _user.errors.to_hash }
        register_params[:validation_token] = _user.validation_token
        expect {
          post :register, params: { account: register_params }
        }.not_to change(User, :count)
        expect(response).to redirect_to signin_path
        expect(response).to have_http_status(:found)
        expect(User.find_by_email(user.email).person_id).to eq(person.id)
      end
      it "shouldn't update the person_id of an user assigned to another person" do
        _user = UnconfirmedUser.new register_params
        expect{ person.save }.not_to change{ person.errors.to_hash }
        expect{ another_person.save }.not_to change{ another_person.errors.to_hash }
        user.email = _user.email
        user.person = another_person
        expect{ Timecop.travel(2.minute.from_now){ user.save } }.not_to change{ user.errors.to_hash }
        _user.person = person
        expect{ Timecop.travel(1.minute.from_now){ _user.save } }.not_to change{ _user.errors.to_hash }
        register_params[:validation_token] = _user.validation_token
        expect {
          post :register, params: { account: register_params }
        }.not_to change(User, :count)
        expect(response).to redirect_to signin_path
        expect(response).to have_http_status(:found)
        expect(User.find_by_email(user.email).person_id).to eq(another_person.id)
      end
      it "should sign in after registration"
    end
  end
end
