FactoryGirl.define do
  sequence :email do |n|
    "me-#{n}@mailinator.com"
  end
  factory :unconfirmed_user do
    name "MyString"
    email
    validation_token "MyString"
    password "123456"
  end

  factory :user do
    name "MyString"
    email
    password "123456"
  end

  factory :person do
    name "My String"
  end
end
