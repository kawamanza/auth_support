require "rails_helper"

module AuthSupport
  RSpec.describe UnconfirmedUserMailer, type: :mailer do
    let(:user){ FactoryGirl.build :unconfirmed_user }
    describe "welcome" do
      let(:mail) { UnconfirmedUserMailer.welcome(user, "http://localhost:3000/register") }

      it "renders the headers" do
        expect(mail.subject).to eq("Welcome")
        expect(mail.to).to eq([user.email])
        expect(mail.from).to eq(["from@example.com"])
      end

      it "renders the body" do
        expect(mail.body.encoded).to match("Hello #{user.name}")
      end
    end

  end
end
