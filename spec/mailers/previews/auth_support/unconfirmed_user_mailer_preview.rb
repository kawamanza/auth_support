module AuthSupport
  # Preview all emails at http://localhost:3000/rails/mailers/auth_support/unconfirmed_user_mailer
  class UnconfirmedUserMailerPreview < ActionMailer::Preview

    # Preview this email at http://localhost:3000/rails/mailers/auth_support/unconfirmed_user_mailer/new_user
    def new_user
      UnconfirmedUserMailer.new_user
    end

  end
end
