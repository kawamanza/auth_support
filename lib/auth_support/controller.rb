module AuthSupport
  module Controller

    def self.included(base)
      base.helper_method :current_user
    end

    protected

    def current_user
      current_session.user
    end

    private

    def current_session
      @session_support ||= ::AuthSupport::Session.new(cookies)
    end

    def authenticate
      redirect_to_login_page unless current_user
    end

    def redirect_to_login_page
      if request.xhr?
        head :unauthorized
      else
        params = {}
        params[:ret_url] = request.original_url if 'GET' == request.method
        redirect_to Engine.routes.url_helpers.signin_path(params)
      end
    end
  end
end
