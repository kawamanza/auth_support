module AuthSupport
  module UserPersistence
    protected

    def confirm_and_create_user params
      _user = UnconfirmedUser.find_by_email params[:email]
      @unconfirmed_user = _user
      if _user && _user.verify_validation_token(params[:password], params[:validation_token])
        @user = user = User.find_by_email(params[:email])
        if user.nil?
          @user = user = User.new params.except(:validation_token)
        else
          user.password = params[:password]
        end
        user.name = params[:name].presence || @unconfirmed_user.name
        user.person_id = _user.person_id if user.person_id.nil?
        ActiveRecord::Base.transaction do
          raise ActiveRecord::Rollback unless user.save && _user.delete
          true
        end
      end
    end
  end
end
