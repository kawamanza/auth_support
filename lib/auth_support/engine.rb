module AuthSupport
  class Engine < ::Rails::Engine
    isolate_namespace AuthSupport
    engine_name :auth_support

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    initializer :append_migrations do |app|
      next unless Rails.root.join("db/migrate").exist?
      has_installed_migrations = Rails.root.join("db/migrate").children(false).
        any?{|f| f.to_s =~/\A\d+_create_unconfirmed_users\.auth_support\.rb\Z/ }
      unless has_installed_migrations || app.root.to_s.match(root.to_s)
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end
  end
end
