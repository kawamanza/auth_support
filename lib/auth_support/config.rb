module AuthSupport
  class Config
    attr_accessor :token_refresh_seconds
    attr_accessor :app_secret_key

    def reset!
      self.tap do |config|
        config.token_refresh_seconds = 2 * 60
        config.app_secret_key = "secret"
      end
    end

    def views_layout=(layout)
      if layout.is_a? Hash
        layout.each_pair do |key, value|
          case key.to_s
          when "identification"
            IdentificationController.layout value
          when "registration"
            RegistrationController.layout value
          when "accounts"
            AccountsController.layout value
          end
        end
      else
        AccountsController.layout layout
        RegistrationController.layout layout
        IdentificationController.layout layout
      end
    end

    def check_robots_strategy=(strategy)
      case strategy
      when :recaptcha
        RegistrationController.send :include, RobotVerification::ReCaptcha
      else
        raise ArgumentError, "Invalid strategy #{ strategy.inspect }"
      end
    end
  end

  CONFIG = Config.new.reset!
end
