module AuthSupport
  module CookieMethods

    protected

    def create_session_cookies!(secret, seq = nil)
      cookies[:_session_token] = { value: secret.cookie_token }
      cookies[:_session_check] = { value: secret.cookie_check(seq), httponly: true }
    end

    def remove_session_cookies!
      cookies.delete :_session_token
      cookies.delete :_session_check
    end

    def refresh_session_cookies!(user)
      secret = Secret.new
      secret.parse(cookies[:_session_token], cookies[:_session_check])
      if :invalid != secret.status
        secret.build_key(user)
        create_session_cookies!(secret)
      end
    end

    def refresh_session_cookie_check!(secret, seq)
      cookies[:_session_check] = { value: secret.cookie_check(seq), httponly: true }
    end
  end
end
