require 'digest/sha2'

module AuthSupport
  class Secret

    USER_FIELDS = %I[id email person_id name].freeze

    attr_reader :token
    attr_reader :timestamp
    attr_reader :seq
    attr_reader :status

    def key; Array.wrap(@key).dup end

    def initialize(secret = CONFIG.app_secret_key)
      @secret = secret
    end

    def parse_key(cookie_token)
      @key = cookie_token.unpack('m').first.force_encoding('UTF-8').split(';')
      @token = @key.shift
    end

    def build_key(user, timestamp = nil)
      @key = USER_FIELDS.map{ |m| user.send(m).to_s.gsub(';', ' ') }
      @session_token = nil
      build_token(timestamp) if timestamp
    end

    def build_token(timestamp)
      @token = Digest::SHA256.hexdigest(([timestamp.to_f] + @key).join(';'))
      @timestamp = timestamp
    end

    def build_user
      if @key && @key.size == USER_FIELDS.size
        User.new.tap do |user|
          USER_FIELDS.each_with_index do |field, index|
            user.send("#{field}=", @key[index])
          end
        end
      end
    end

    def sign(data, seq)
      OpenSSL::HMAC.hexdigest OpenSSL::Digest.new('sha1'), signature_key(seq), data
    end

    def parse(cookie_token, cookie_check, maxage = CONFIG.token_refresh_seconds)
      @status = :invalid
      parse_key(cookie_token)
      parts = cookie_check.split('-')
      @timestamp = Time.at parts[0].to_i(16)
      @seq = parts[1].to_i
      if sign(session_token, seq) == parts[2]
        @status = :valid
        if Time.now.to_i > timestamp.to_i + maxage
          @status, @timestamp = :stale, Time.now
        end
      end
    rescue
      @key, @seq, @timestamp, @status = nil, nil, nil, :invalid
    end

    def session_token
      @session_token ||= ([token] + @key).join(';')
    end

    def cookie_token
      [session_token].pack('m') # base64
    end

    def cookie_check(seq = nil)
      seq = @seq = (seq.nil? ? @seq : seq).to_i
      signature = sign(session_token, seq)
      [timestamp.to_i.to_s(16), seq, signature].join('-')
    end

    private

    def signature_key(seq)
      [@secret, timestamp.to_i, seq].join('_')
    end
  end
end
