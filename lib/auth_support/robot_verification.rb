module AuthSupport
  module RobotVerification
    module ReCaptcha
      def is_robot?(resource)
        verify_recaptcha(model: resource)
      end
    end
  end
end
