module AuthSupport
  class Session
    include CookieMethods

    attr_reader :cookies

    def initialize(cookies)
      @cookies = cookies
    end

    def user
      (
        @_user ||= user_from_token \
          cookies[:_session_token],
          cookies[:_session_check]
      ).first
    end

    private

    def user_from_token(cookie_token, cookie_check)
      secret = Secret.new
      secret.parse(cookie_token, cookie_check)
      return [nil] unless user = secret.build_user
      status, seq = secret.status, secret.seq
      if :stale == status && valid_session?(secret.token, user.email, seq)
        refresh_session_cookie_check!(secret, seq+1)
        status = :valid
      end
      if :valid != status
        remove_session_cookies!
        AuthenticatedSession.delete_session(secret.token, user.email)
        user = nil
      end
      [user]
    end

    def valid_session?(token, email, prev_seq)
      AuthenticatedSession.increase_seq(token, email, prev_seq) > 0
    end
  end
end
