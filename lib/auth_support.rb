require "auth_support/engine"

module AuthSupport
  autoload :UserPersistence, 'auth_support/user_persistence'
  autoload :Secret, 'auth_support/secret'
  autoload :Controller, 'auth_support/controller'
  autoload :CookieMethods, 'auth_support/cookie_methods'
  autoload :RobotVerification, 'auth_support/robot_verification'
  autoload :Session, 'auth_support/session'

  autoload :CONFIG, 'auth_support/config'
  autoload :Config, 'auth_support/config'

  def self.configure(&block)
    yield CONFIG if block_given?
  end
end
