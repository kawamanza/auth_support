require 'rails/generators/base'

module AuthSupport
  module Generators
    # Include this module in your generator to generate AuthSupport views.
    # `copy_views` is the main method and by default copies all views
    # with forms.
    module ViewPathTemplates #:nodoc:
      extend ActiveSupport::Concern

      included do
        class_option :views, aliases: "-v", type: :array, desc: "Select specific view directories to generate (identification, registration, unconfirmed_user_mailer)"
        public_task :copy_views
      end

      module ClassMethods
        def hide!
          Rails::Generators.hide_namespace self.namespace
        end
      end

      def copy_views
        if options[:views]
          options[:views].each do |directory|
            view_directory directory.to_sym
          end
        else
          view_directory :identification
          view_directory :registration
          view_directory :unconfirmed_user_mailer
        end
      end

      protected

      def view_directory(name, _target_path = nil)
        directory name.to_s, _target_path || "app/views/auth_support/#{name}"
      end
    end

    class ViewsGenerator < Rails::Generators::Base #:nodoc:
      include ViewPathTemplates
      source_root File.expand_path("../../../../app/views/auth_support", __FILE__)
      desc "Copies default AuthSupport views to your application."
      # hide!
    end
  end
end
