class CreateUserAuthlogs < ActiveRecord::Migration
  def change
    create_table :user_authlogs, id: false do |t|
      t.references :user, index: true, foreign_key: true
      t.datetime :signin_at, null: false
      t.string :signin_ip, limit: 32, null: false
      t.boolean :failed_attempt, null: false, default: true
    end
  end
end
