class CreateUnconfirmedUsers < ActiveRecord::Migration
  def change
    create_table :unconfirmed_users, primary_key: :email do |t|
      t.string :name, limit: 150, null: false
      t.references :person, index: true, foreign_key: true
      t.string :validation_token, limit: 64, null: false

      t.timestamps null: false
    end
    change_column :unconfirmed_users, :email, :string, limit: 200
  end
end
