class CreateAuthenticatedSessions < ActiveRecord::Migration
  def change
    create_table :authenticated_sessions, id: false do |t|
      t.string :email, limit: 200, null: false
      t.references :user, index: true, foreign_key: true
      t.string :token, limit: 150, null: false
      t.integer :seq, null: false, default: 0

      t.timestamps null: false
    end
  end
end
