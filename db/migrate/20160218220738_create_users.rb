class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.references :person, index: true, foreign_key: true
      t.string :name, limit: 150, null: false
      t.string :email, limit: 200, null: false
      t.string :encrypted_password, limit: 100, null: false
      t.string :situation, limit: 1, null: false, default: "A"

      t.timestamps null: false
    end
  end
end
