class AddAcceptedContractToUsers < ActiveRecord::Migration
  def change
    add_column :users, :accepted_contract, :integer
  end
end
